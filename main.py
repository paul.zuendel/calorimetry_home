#############################################
#Praktikum Digitalisierung
#Kalorimetrie - Küchentischversuch
#Paul Zuendel
#Gruppe 99
#############################################

from functions import m_json
from functions import m_pck

path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)

m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets/", metadata)
print(metadata)

data = m_pck.get_meas_data_calorimetry(metadata)
m_pck.logging_calorimetry(data, metadata, "/home/pi/calorimetry_home/data/", "/home/pi/calorimetry_home/datasheets/")
m_json.archiv_json("/home/pi/calorimetry_home/datasheets/", path, "/home/pi/calorimetry_home/data/")